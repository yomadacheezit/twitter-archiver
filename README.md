<<<<<<< README.md
# TwitterArchiver
This is a tool user to download an archive of a persons tweets, retweets, and likes
for the sake of archival. Its important that a service like this exists, and is easy to 
use for most users. It gives users data through a excel file

## Requirements
    - nest_asyncio
    - snscrape
    - pandas
    - openpyxl
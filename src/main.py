#!/usr/bin/env python3
import os
import random

from flask import Flask, render_template, request, send_file
import snscrape.modules.twitter as sntwitter
import pandas as pd

import nest_asyncio


nest_asyncio.apply()

#Initalize the flask server with the appropriate stuff
template_dir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
template_dir = os.path.join(template_dir, 'templates')
app = Flask(__name__, template_folder=template_dir)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/archive', methods=['POST'])
def archive():
    username = request.form["username"]

    try:
        scraper = sntwitter.TwitterUserScraper(username)
        scraper._get_entity()
    except ValueError:
        print("Invalid user!")
        return render_template('invalid-user.html')

    print(f"Archiving user: {username}")

    Tweets_dfsept = []
    for i,tweetsept in enumerate(sntwitter.TwitterUserScraper(username).get_items()):
        print(tweetsept)
        
        Tweets_dfsept.append(tweetsept)

    #Turn tweets into a excel file
    Tweets_dfsept2 = pd.DataFrame(Tweets_dfsept)
    print(Tweets_dfsept2)
    Tweets_dfsept2['date'] = Tweets_dfsept2['date'].apply(lambda a: pd.to_datetime(a).date()) 
    Tweets_dfsept2.to_excel(f'./temp/{username}.xlsx')

    #Send the excel file
    return send_file(f'../temp/{username}.xlsx')


app.run(host='0.0.0.0', port=8080, threaded=True)